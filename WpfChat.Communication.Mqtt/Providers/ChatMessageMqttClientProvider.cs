﻿using MQTTnet;
using MQTTnet.Extensions.ManagedClient;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using WpfChat.Communication.Mqtt.Entities;
using WpfChat.Communication.Mqtt.Settings;

namespace WpfChat.Communication.Mqtt.Providers
{
    public class ChatMessageMqttClientProvider : BaseMqttClientProvider<ChatMessage>
    {
        protected override string TopicPrefix => "WpfChatQueue";

        public ChatMessageMqttClientProvider(IMqttConnectionSettings settings)
            : base(settings)
        {

        }

        public override Task PublishAsync(ChatMessage message)
        {
            var messageTopic = $"{this.TopicPrefix}/{message.Channel}";
            var jsonString = JsonConvert.SerializeObject(message);
            var bytes = Encoding.ASCII.GetBytes(jsonString);

            return this.MqttClient.PublishAsync(new MqttApplicationMessageBuilder()
                .WithTopic(messageTopic)
                .WithPayload(bytes)
                .Build());
        }

        public override Task SubscribeAsync(string topic = "#")
        {
            var subTopic = $"{this.TopicPrefix}/{topic}";

            return this.MqttClient.SubscribeAsync(new MqttTopicFilterBuilder()
                .WithTopic(subTopic)
                .Build());
        }

        public override Task UnSubscribeAsync(string topic = "#")
        {
            var subTopic = $"{this.TopicPrefix}/{topic}";

            return this.MqttClient.UnsubscribeAsync(subTopic);
        }

        protected override void OnClientMessageReceived(MqttApplicationMessageReceivedEventArgs obj)
        {
            if (obj.ApplicationMessage.Payload != null)
            {
                var jsonString = Encoding.ASCII.GetString(obj.ApplicationMessage.Payload);
                var message = JsonConvert.DeserializeObject<ChatMessage>(jsonString);
                OnMessage?.Invoke(message);
            }
        }
    }
}
