﻿using MQTTnet;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Options;
using MQTTnet.Client.Receiving;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Formatter;
using System;
using System.Text;
using System.Threading.Tasks;
using WpfChat.Communication.Mqtt.Exceptions;
using WpfChat.Communication.Mqtt.Settings;

namespace WpfChat.Communication.Mqtt
{
    public abstract class BaseMqttClientProvider<T> : IMqttClientProvider<T>
    {
        public Action<T> OnMessage { get; set; }
        public bool IsStarted => this.MqttClient.IsStarted;
        public bool IsConnected => this.MqttClient.IsConnected;
        protected IManagedMqttClient MqttClient { get; private set; }
        protected abstract string TopicPrefix { get; }
        private readonly IManagedMqttClientOptions mqttOptions;
        private bool disposedValue;

        protected BaseMqttClientProvider(IMqttConnectionSettings settings)
        {
            this.mqttOptions = new ManagedMqttClientOptions()
            {
                ClientOptions = new MqttClientOptions
                {
                    ClientId = $"{nameof(T)}_{Guid.NewGuid()}",
                    Credentials = new MqttClientCredentials
                    {
                        Username = settings.Username,
                        Password = Encoding.ASCII.GetBytes(settings.Password)
                    },
                    ProtocolVersion = MqttProtocolVersion.V311,
                    CleanSession = true,
                    CommunicationTimeout = TimeSpan.FromSeconds(5),
                    KeepAlivePeriod = TimeSpan.FromSeconds(5),
                    ChannelOptions = new MqttClientTcpOptions
                    {
                        Server = settings.Address,
                        Port = settings.Port
                    }
                },
            };

            this.MqttClient = new MqttFactory().CreateManagedMqttClient();
            this.MqttClient.ConnectedHandler = new MqttClientConnectedHandlerDelegate(OnClientConnected);
            this.MqttClient.DisconnectedHandler = new MqttClientDisconnectedHandlerDelegate(OnClientDisconnected);
            this.MqttClient.ConnectingFailedHandler = new ConnectingFailedHandlerDelegate(OnConnectingFailed);
            this.MqttClient.ApplicationMessageReceivedHandler = new MqttApplicationMessageReceivedHandlerDelegate(OnClientMessageReceived);
        }

        /// <summary>
        /// Starts MQTT subscriber.
        /// </summary>
        public async Task ConnectAsync()
        {
            await this.MqttClient.StartAsync(this.mqttOptions);
        }

        public async Task DisconnectAsync()
        {
            await this.MqttClient.StopAsync();
        }

        public abstract Task SubscribeAsync(string topic = "#");
        public abstract Task UnSubscribeAsync(string topic = "#");

        public abstract Task PublishAsync(T message);
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected abstract void OnClientMessageReceived(MqttApplicationMessageReceivedEventArgs obj);
        private void OnClientConnected(MqttClientConnectedEventArgs obj) { }

        private async Task OnClientDisconnected(MqttClientDisconnectedEventArgs e)
        {
            await this.MqttClient.StartAsync(this.mqttOptions);
        }

        private void OnConnectingFailed(ManagedProcessFailedEventArgs obj)
        {
            throw new MqttConnectionException("Cannot connect to MQTT Broker", obj.Exception);
        }

        private async void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (this.MqttClient.IsStarted)
                        await this.MqttClient.StopAsync();

                    this.MqttClient.Dispose();
                }

                disposedValue = true;
            }
        }
    }
}
