﻿using System;

namespace WpfChat.Communication.Mqtt.Entities
{
    public class ChatMessage
    {
        public string Username { get; set; }
        public DateTime SendDate { get; set; }
        public string Content { get; set; }
        public string Channel { get; set; }
    }
}
