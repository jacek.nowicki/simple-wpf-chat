﻿namespace WpfChat.Communication.Mqtt.Settings
{
    /// <summary>
    /// Represents the settings used to connect to the MQTT broker.
    /// </summary>
    public interface IMqttConnectionSettings
    {
        /// <summary>
        /// Address of the MQTT broker.
        /// </summary>
        string Address { get; set; }

        /// <summary>
        /// Port used to connect with MQTT broker.
        /// </summary>
        int Port { get; set; }

        /// <summary>
        /// Username required when connecting to secured brokers.
        /// </summary>
        string Username { get; set; }

        /// <summary>
        /// Password required when connecting to secured brokers.
        /// </summary>
        string Password { get; set; }
    }
}