﻿namespace WpfChat.Communication.Mqtt.Settings
{
    /// <summary>
    /// Represents the settings used to connect to the MQTT broker.
    /// </summary>
    public class MqttConnectionSettings : IMqttConnectionSettings
    {
        /// <summary>
        /// Address of the MQTT broker.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Port used to connect with MQTT broker.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Username required when connecting to secured brokers.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Password required when connecting to secured brokers.
        /// </summary>
        public string Password { get; set; }
    }
}
