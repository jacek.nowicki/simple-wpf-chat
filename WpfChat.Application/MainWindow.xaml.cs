﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfChat.Communication.Mqtt.Entities;
using WpfChat.Communication.Mqtt.Providers;
using WpfChat.Communication.Mqtt.Settings;

namespace WpfChat.Application
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ChatMessageMqttClientProvider chatProvider;
        private string username;
        private string channel;

        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(string username, string channel)
        {
            // Initialize MainWindow
            InitializeComponent();
            this.username = username;
            this.channel = channel;

            // Initialize MQTT Provider
            var mqttSettings = new MqttConnectionSettings()
            {
                Address = "test.mosquitto.org",
                Port = 1883,
                Password = "",
                Username = ""
            };
            chatProvider = new ChatMessageMqttClientProvider(mqttSettings);
            chatProvider.ConnectAsync();
            chatProvider.SubscribeAsync(channel);
            chatProvider.OnMessage = ReceivedChatMessage;
        }

        private void SendBtn_Click(object sender, RoutedEventArgs e)
        {
            var message = new ChatMessage
            {
                Username = username,
                Channel = channel,
                Content = MessageTb.Text,
                SendDate = DateTime.Now
            };

            chatProvider.PublishAsync(message);
        }

        private void ReceivedChatMessage(ChatMessage message)
        {
            var text = $"[{message.SendDate.ToString("HH:mm")}] <{message.Username}> {message.Content}";

            MessagesSp.Items.Add(text);
        }
    }
}
